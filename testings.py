import torch

from datetime import date
from torch import optim
from rlagent.env import LampEnv
from rlagent.policy import  ReinforceWithBaseline
from rlagent.schedule import Schedule

device = torch.device("cpu")


schedule = Schedule(start_date=date(2017, 12, 15), stop_date=date(2018, 5, 1), first_set_date=date(2018, 2, 1),
                 last_set_date=date(2018, 2, 5), max_plant_load=350)

env = LampEnv(schedule)

agent = ReinforceWithBaseline(schedule).to(device)
optimizer = optim.Adam(agent.parameters(), lr=1e-2)


n_episodes = 500

scores = []
for i_episode in range(1, n_episodes + 1):
    saved_log_probs = []; rewards = []; observations = []
    done = False

    obs = env.reset()
    while not done:
        action, log_prob = agent.lamp_act(obs)
        observations.append(obs)
        saved_log_probs.append(log_prob)

        obs, reward, done, _ = env.step(action)
        rewards.append(reward)

    policy_loss = []; value_loss = []
    for idx, log_prob in enumerate(saved_log_probs):
        delta = sum(rewards[idx:]) - agent.state_value(observations[idx])
        policy_loss.append(-log_prob * delta)
        value_loss.append((observations[idx], delta))

    for obs, d in value_loss:
        agent.update_state_value(obs, d)

    policy_loss = torch.cat(policy_loss).sum()
    optimizer.zero_grad()
    policy_loss.backward()
    optimizer.step()

    print('Episode: %s, Score: %s' % (i_episode, sum(rewards)))

print()


# env.step(1)
# print()


#
# agent = QLearningAgent(schedule)
#
# num_episodes = 100
#
# scores = []
# for i_episode in tqdm(range(1, num_episodes + 1)):
#     # Initialize episode
#     state = env.reset()
#     action = agent.reset_episode(state)
#     total_reward = 0
#     done = False
#
#     print('Step: %s, Action: %s, State: %s' % (agent.step, action, state))
#     # Roll out steps until done
#     while not done:
#         state, reward, done, info = env.step(action)
#         total_reward += reward
#         action = agent.lamp_act(state, reward, done, AgentType.Train)
#         print('Step: %s, Action: %s, State: %s, Reward: %.2f' % (agent.step, action, state, reward))
#
#     # Save final score
#     print()
#     scores.append(total_reward)
#
# print("State space:", env.observation_space)
# print("- low:", env.observation_space.low)
# print("- high:", env.observation_space.high)