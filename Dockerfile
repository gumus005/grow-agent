FROM python:3.6

COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt

COPY . /home/app
WORKDIR /home/app

CMD ["python", "main.py"]
