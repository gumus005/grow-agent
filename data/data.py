import json
import os

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

with open(os.path.join(__location__, 'data.json')) as data_file:
    data = data_file.read()
    example = json.loads(data)

example_par = example['par']
example_harvest = example['harvest']