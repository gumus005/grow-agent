default_parameters = {
    "simset": {
        "@startDate": "15-12-2018",
        "@endDate": "20-05-2019"
    },
    "common": {
        "CO2dosing": {
            "@pureCO2cap": 150
        }
    },
    "comp1": {
        "setpoints": {
            "temp": {
                "@heatingTemp": 18,
                "@ventOffset": 2,
                "@radiationInfluence": "100 400 0",
                "@radiationInfluenceC": "100 400 0",
                "@PbandVent": "6 18; 20 4"
            },
            "ventilation": {
                "@startWnd": 30,
                "@winLeeMin": 0,
                "@winLeeMax": 100,
                "@winWndMin": 0,
                "@winWndMax": 100
            },
            "CO2": {
                "@setpoint": 500,
                "@setpIfLamps": 700,
                "@doseCapacity": 100
            }
        },
        "heatingpipes": {
            "pipe1": {
                "@maxTemp": 80,
                "@minTemp": 0,
                "@radiationInfluence": "100  400"
            }
        },
        "screens": {
            "scr1": {
                "@enabled": True,
                "@material": "scr_OBSCURA_9950_FR.par",
                "@ToutMax": 15,
                "@closeBelow": 5,
                "@closeAbove": 1200,
                "@lightPollutionPrevention": True
            },
            "scr2": {
                "@enabled": True,
                "@material": "scr_LUXOUS_1347_FR.par",
                "@ToutMax": 15,
                "@closeBelow": "0 290; 10 2",
                "@closeAbove": 1200,
                "@lightPollutionPrevention": False
            }
        },
        "illumination": {
            "lmp1": {
                "@enabled": True,
                "@intensity": 200,
                "@hoursLight": 18,
                "@endTime": 20,
                "@maxIglob": 150,
                "@maxPARsum": 18
            }
        },
        "airconditioning": {
            "fogging": {
                "@enabled": False,
                "@capacity": 50,
                "@minTemp": 0,
                "@RHstart": 75
            }
        },
        "irrigation": {
            "@includeWaterstress": False,
            "@shotSize": 400,
            "@molesPerShot": 2.2,
            "@maxPauseTime": 2,
            "@EC": 2.5
        }
    },
    "crp_tomato_cherry_Axiany": {
        "general": {
            "@productPrice": "7 4; 12 5"
        },
        "Intkam": {
            "management": {
                "@stemDensity": "1 1.5; 20 2.25; 50 3",
                "@leafPickingStrategy": "1 0; 25 0.33; 80 0.16",
                "@dayTopping": 120
            },
            "LAI": {
                "@targetLAI": 3
            },
            "tomato": {
                "growth": {
                    "@FruitNrPerTruss": "1 10; 70 15"
                }
            }
        }
    }
}

# no_increase_parameters = {
#     "simset": {
#         "@startDate": "15-12-2018",
#         "@endDate": "20-05-2019"
#     },
#     "common": {
#         "CO2dosing": {
#             "@pureCO2cap": 150
#         }
#     },
#     "comp1": {
#         "setpoints": {
#             "temp": {
#                 "@heatingTemp": 18,
#                 "@ventOffset": 2,
#                 "@radiationInfluence": "100 400 0", # Change in here
#                 "@radiationInfluenceC": "100 400 0",
#                 "@PbandVent": "6 18; 20 4"
#             },
#             "ventilation": {
#                 "@startWnd": 30,
#                 "@winLeeMin": 0,
#                 "@winLeeMax": 100,
#                 "@winWndMin": 0,
#                 "@winWndMax": 100
#             },
#             "CO2": {
#                 "@setpoint": 500,
#                 "@setpIfLamps": 700,
#                 "@doseCapacity": 100
#             }
#         },
#         "heatingpipes": {
#
#             "pipe1": {
#                 "@maxTemp": 80,
#                 "@minTemp": 0,
#                 # "@radiationInfluence": "100  400" # Change in here
#             }
#         },
#         "screens": {
#             "scr1": {
#                 "@enabled": True,
#                 "@material": "scr_OBSCURA_9950_FR.par",
#                 "@ToutMax": 15,
#                 "@closeBelow": 5,
#                 "@closeAbove": 1200,
#                 "@lightPollutionPrevention": True
#             },
#             "scr2": {
#                 "@enabled": True,
#                 "@material": "scr_LUXOUS_1347_FR.par",
#                 "@ToutMax": 15,
#                 "@closeBelow": "0 290; 10 2",
#                 "@closeAbove": 1200,
#                 "@lightPollutionPrevention": False
#             }
#         },
#         "illumination": {
#             "lmp1": {
#                 "@enabled": True,
#                 "@intensity": 200,
#                 "@hoursLight": 18
#                # "@endTime": 20,
#                # "@maxIglob": 150,  # Change in here
#                # "@maxPARsum": 18   # Change in here
#             }
#         },
#         "airconditioning": {
#             "fogging": {
#                 "@enabled": False,
#                 "@capacity": 50,
#                 "@minTemp": 0,
#                 "@RHstart": 75
#             }
#         },
#         "irrigation": {
#             "@includeWaterstress": False,
#             "@shotSize": 400,
#             "@molesPerShot": 2.2,
#             "@maxPauseTime": 2,
#             "@EC": 2.5
#         }
#     },
#     "crp_tomato_cherry_Axiany": {
#         "general": {
#             "@productPrice": "7 4; 12 5"
#         },
#         "Intkam": {
#             "management": {
#                 "@stemDensity": "1 1.5; 20 2.25; 50 3",
#                 "@leafPickingStrategy": "1 0; 25 0.33; 80 0.16",
#                 "@dayTopping": 120
#             },
#             "LAI": {
#                 "@targetLAI": 3
#             },
#             "tomato": {
#                 "growth": {
#                     "@FruitNrPerTruss": "1 10; 70 15"
#                 }
#             }
#         }
#     }
# }