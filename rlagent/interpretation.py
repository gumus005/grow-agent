import numpy as np


def harvest_cumulative_day(harvest):
    harvest_h = harvest_hours(harvest)
    periods = list(zip(harvest_h[:-1], harvest_h[1:]))
    return np.hstack([np.cumsum(np.ones(h2-h1)) for h1, h2 in periods]).tolist()


def harvest_cumulative_par(harvest, par):
    harvest_h = harvest_hours(harvest)
    periods = list(zip(harvest_h[:-1], harvest_h[1:]))
    return np.hstack([np.cumsum(par[h1:h2]) for h1, h2 in periods]).tolist()


def harvest_cumulative_temp(harvest, temp):
    harvest_h = harvest_hours(harvest)
    periods = list(zip(harvest_h[:-1], harvest_h[1:]))
    return np.hstack([np.cumsum(temp[h1:h2]) for h1, h2 in periods]).tolist()


def flat_plant_load(plant_load):
    pl1 = np.array(plant_load).reshape((-1, 24))
    pl2 = np.roll(np.tile(pl1[:, 0], (24, 1)).T, -1, axis=0).flatten()
    return pl2


def harvest_hours(harvest):
    delta_harvest = np.diff([0] + harvest)
    h1 = np.array(delta_harvest).reshape((-1, 24))
    h2 = np.sum(h1, 1).reshape((-1, 1))
    h3 = np.hstack((h2, np.zeros((h2.shape[0], 23))))
    h4 = h3.flatten()
    h5 = [0] + np.where(h4 > 0)[0].tolist()
    return h5


def harvest_count(harvest, day):
    return sum(np.array(harvest_hours(harvest)) < day)


def linearized_harvest(harvest: list, gains: float):
    delta_harvest = np.diff([0] + harvest)
    h1 = np.array(delta_harvest).reshape((-1, 24))
    h2 = np.sum(h1, 1).reshape((-1, 1))
    h3 = np.hstack((h2, np.zeros((h2.shape[0], 23))))
    h4 = h3.flatten()
    h5 = [0] + np.where(h4 > 0)[0].tolist()
    h6 = h4[h4>0].tolist()
    h7 = np.hstack([hr/(en-st)]*(en-st) for st, en, hr in zip(h5[:-1], h5[1:], h6))
    return (np.cumsum((h7 / sum(h7))) * gains).tolist()


def last_harvest_idx(harvest: list):
    harvest_idxes = np.where(np.diff([0] + harvest) > 0)[0]
    return harvest_idxes[-1]


if __name__ == '__main__':
    from data.data import example_par
    from data.data import example_harvest

    harvest_hours = harvest_hours(example_harvest)
    print()
    # print(cumulative_par(example_harvest, example_par))
    # print()
    #linear_harvest(example_harvest, 3000, )

