from datetime import datetime

from rlagent.parameters import default_parameters
from rlagent.schedule import Schedule

import xlrd
import requests
import json
import numpy as np


def time2elec_usage(time, lamps, peaks):
    day_idx = time // 24
    day_hour = time % 24

    light_hour = lamps[day_idx]
    is_peak = 2 if peaks[time] == 1 else 1
    enabled = 1 if day_hour < 22 and (light_hour >= 22 - day_hour) else 0

    return enabled * is_peak


def xldate_to_datetime(xldate):
    python_date = datetime(*xlrd.xldate_as_tuple(xldate - 1 / 24, 0))
    return python_date


def get_simulation_results(temps, lamps, schedule: Schedule):
    all_lamps = schedule.lamps2all_lamps(lamps)
    all_temps = schedule.temps2all_temps(temps)

    heating_parameter = schedule.temps2parameter(all_temps)
    hours_parameter, endParameter = schedule.lamps2parameter(all_lamps)

    key = 'Orcun-2315-aqb15'
    url = "https://www.digigreenhouse.wur.nl/AGC/"

    parameters = default_parameters

    parameters['simset']['@startDate'] = schedule.start_date_str
    parameters['simset']['@endDate'] = schedule.stop_date_str
    parameters['comp1']['setpoints']['temp']['@heatingTemp'] = heating_parameter
    parameters['comp1']['illumination']['lmp1']['@hoursLight'] = hours_parameter


    headers = {}

    payload = {
        'key': key,
        'parameters': json.dumps(parameters)
    }

    response = requests.request("POST", url, data=payload, headers=headers)
    json_response = response.json()

    datetimes = [(dt.month, dt.day, dt.hour) for dt in
                 [xldate_to_datetime(i) for i in json_response['data']['DateTime']['data']]]

    lai = json_response['data']['comp1.LAI.Value']['data']
    temp_set_points = json_response['data']['comp1.Setpoints.SpHeat']['data']
    harvest = json_response['data']['comp1.Harvest.CumFruitsFW']['data']
    peaks = json_response['data']['common.Economics.PeakHour']['data']

    par = json_response['data']['comp1.PARsensor.Above']['data']
    Iglob = json_response['data']['common.Iglob.Value']['data']
    TOut = json_response['data']['common.TOut.Value']['data']
    plant_load = json_response['data']['comp1.Plant.PlantLoad']['data']

    lamp_cost = json_response['stats']['economics']['variableCosts']['objects']['comp1.Lmp1']
    temp_cost = json_response['stats']['economics']['variableCosts']['objects']['comp1.Pipe1']

    gains = json_response['stats']['economics']['gains']['total']

    pipe_usage = np.array(json_response['data']['comp1.PConPipe1.Value']['data'])
    pipe_usage = (pipe_usage / sum(pipe_usage)) * temp_cost

    lamp_usage = np.array([time2elec_usage(t, all_lamps, peaks) for t in range(len(datetimes))])
    lamp_usage = (lamp_usage / sum(lamp_usage)) * lamp_cost

    output = {
        "datetimes": datetimes,
        "par": par,
        "Iglob": Iglob,
        "gains": gains,
        "harvest": harvest,
        "plant_load": plant_load,
        "temps_set_points": temp_set_points,
        "lamps_set_points": all_lamps,
        "lamp_usage": np.cumsum(lamp_usage).tolist(),
        "pipe_usage": np.cumsum(pipe_usage).tolist()
    }
    return output



# def generate_lamps(policy, schedule):
#     """
#     input: policy function
#     returns: [18, 18 ...]
#     """
#
#     state = (schedule.set_temp_min_index, LAMP_DEFAULT)
#     day_idx, lamp = policy.lamp_act(state, ep)
#     lamps = [LAMP_DEFAULT, lamp]
#
#     for _ in SET_DATE_INDEXES[2:]:
#         state = (day_idx, lamp)
#         day_idx, lamp = policy.lamp_act(state, ep)
#         lamps.append(lamp)
#     return lamps


if __name__ == '__main__':
    from datetime import date

    schedule = Schedule(start_date=date(2017, 12, 15), stop_date=date(2018, 3, 30), first_set_date=date(2018, 3, 9), last_set_date=date(2018, 3, 15))
    output = get_simulation_results([18, 18, 18, 18, 18, 18, 18, 18] * 6, [18] * 6, schedule)

    output = {
        'harvest': output['harvest'],
        'par': output['par']
    }
    with open("./data.json", "w") as data_file:
        json.dump(output, data_file, indent=2)