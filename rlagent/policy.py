from collections import defaultdict
from random import choice

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Categorical
from enum import Enum


class AgentType(Enum):
    Learner = 1
    Train = 2


class ReinforceWithBaseline(nn.Module):
    def __init__(self, schedule, h_size=16, alpha=0.1):
        super(ReinforceWithBaseline, self).__init__()
        self.alpha = alpha
        self.schedule = schedule
        self.device = torch.device("cpu")

        self.values = [0] * len(schedule.set_dates)

        self.fc1 = nn.Linear(2, h_size)
        self.fc2 = nn.Linear(h_size, len(self.schedule.lamp_actions))

    def state_value(self, obs):
        idx, hc, pl, cumulative_par = obs
        return self.values[idx]

    def update_state_value(self, obs, delta):
        idx, hc, pl, cumulative_par = obs
        self.values[idx] += self.alpha * delta

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)

    def lamp_act(self, obs):
        idx, hc, pl, cumulative_par = obs
        standardized_obs = np.array([pl / self.schedule.max_plant_load, cumulative_par / self.schedule.max_cumulative_par])
        standardized_obs = torch.from_numpy(standardized_obs).float().unsqueeze(0).to(self.device)
        probs = self.forward(standardized_obs).cpu()
        m = Categorical(probs)
        action = m.sample()
        return action.item(), m.log_prob(action)


class Reinforce(nn.Module):
    def __init__(self, schedule, h_size=16):
        super(Reinforce, self).__init__()
        self.schedule = schedule
        self.device = torch.device("cpu")

        self.fc1 = nn.Linear(2, h_size)
        self.fc2 = nn.Linear(h_size, len(self.schedule.lamp_actions))

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)

    def lamp_act(self, state):
        pl, cumulative_par = state
        state = np.array([pl / self.schedule.max_plant_load, cumulative_par / self.schedule.max_cumulative_par])
        state = torch.from_numpy(state).float().unsqueeze(0).to(self.device)
        probs = self.forward(state).cpu()
        m = Categorical(probs)
        action = m.sample()
        return action.item(), m.log_prob(action)


class QTable:
    """Simple Q-table."""

    def __init__(self, state_grid, harvest_count, action_size, alpha=0.1, beta=0.1):
        self.alpha = alpha
        self.beta = beta

        self.state_size = tuple([harvest_count, *(len(splits) + 1 for splits in state_grid)])
        self.action_size = action_size
        self.lamp_q_table = np.zeros(shape=(self.state_size + (action_size,)))

    def update_lamp(self, state, action, delta):
        self.lamp_q_table[state + (action,)] += self.alpha * delta


class QLearningAgent:

    def __init__(self, schedule, epsilon=1, epsilon_decay_rate=0.9, min_epsilon=0.15):
        self.schedule = schedule
        self.epsilon = self.initial_epsilon = epsilon
        self.epsilon_decay_rate = epsilon_decay_rate
        self.min_epsilon = min_epsilon

        self.lows = [self.schedule.min_plant_load, self.schedule.min_cumulative_par]
        self.highs = [self.schedule.max_plant_load, self.schedule.max_cumulative_par]

        self.state_grid = self._create_uniform_grid(self.lows, self.highs, bins=(5, 5))
        self.q_table = QTable(state_grid=self.state_grid, harvest_count=schedule.max_harvest_count - schedule.min_harvest_count, action_size=len(schedule.lamp_actions))

        self.step = 0
        self.last_state = None
        self.last_action = None

    @staticmethod
    def _discretize(sample, grid):
        return list(int(np.digitize(s, g)) for s, g in zip(sample, grid))  # apply along each dimension

    @staticmethod
    def _create_uniform_grid(lows, highs, bins):
        grid = [np.linspace(lows[dim], highs[dim], bins[dim] + 1)[1:-1] for dim in range(len(bins))]
        return grid

    def obs2state(self, obs):
        idx, harvest_count, pl, cumulative_par = obs
        return tuple([harvest_count, *self._discretize((pl, cumulative_par), self.state_grid)])

    def reset_episode(self, obs):
        self.step = 0

        self.epsilon *= self.epsilon_decay_rate
        self.epsilon = max(self.epsilon, self.min_epsilon)

        self.last_state = self.obs2state(obs)
        self.last_action = np.argmax(self.q_table.lamp_q_table[self.last_state])

        return self.last_action

    def learn_lamp(self, obs, action, g):
        state = self.obs2state(obs)
        value = self.q_table.lamp_q_table[state + (action,)]

        delta = g - value
        self.q_table.update_lamp(state, action, delta)

        # average_delta = reward - self.q_table.lamp_average
        # self.q_table.update_lamp_average(average_delta)

    def lamp_act(self, obs):

        state = self.obs2state(obs)

        do_exploration = np.random.uniform(0, 1) < self.epsilon
        if do_exploration:
            # Pick a random action
            action = np.random.randint(0, self.q_table.action_size)
        else:
            # Pick the best action from Q table
            action = np.argmax(self.q_table.lamp_q_table[state])

        self.last_state = state
        self.last_action = action
        return action
