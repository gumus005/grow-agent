from datetime import timedelta


def date2simulation_str(d):
    return d.strftime("%d-%m")


class Schedule:
    def __init__(self, start_date, stop_date, first_set_date, last_set_date, hours_period=3, default_temp=24, default_lamp_hour=16, default_lamp_end=22, max_par=500, max_plant_load=200, max_cumulative_par=30000):
        self.hours_period = hours_period
        self.start_date = start_date
        self.stop_date = stop_date
        self.first_set_date = first_set_date
        self.last_set_date = last_set_date

        self.default_temp = default_temp
        self.default_lamp_hour = default_lamp_hour
        self.default_lamp_end = default_lamp_end

        self.temp_actions = [18, 24]
        self.lamp_actions = [4, 16]

        self.min_harvest_count = 3
        self.max_harvest_count = 15

        self.min_plant_load = 120
        self.max_plant_load = max_plant_load

        self.min_cumulative_par = 0
        self.max_cumulative_par = max_cumulative_par

        self.min_par = 0
        self.max_par = max_par

    @property
    def daily_temp_set_point(self):
        return 24 // self.hours_period

    @property
    def temp_indexes(self):
        return list(range(0, len(self.dates) * self.daily_temp_set_point))

    @property
    def lamp_indexes(self):
        return list(range(0, len(self.dates)))

    @property
    def start_date_str(self):
        return self.start_date.strftime("%d-%m-%Y")

    @property
    def stop_date_str(self):
        return self.stop_date.strftime("%d-%m-%Y")

    @property
    def set_temp_min_index(self):
        first_set_day = (self.first_set_date - self.start_date).days
        return first_set_day * self.daily_temp_set_point

    @property
    def set_temp_max_index(self):
        last_set_day = (self.last_set_date - self.start_date).days
        return last_set_day * self.daily_temp_set_point

    @property
    def set_lamp_min_index(self):
        first_set_day = (self.first_set_date - self.start_date).days
        return first_set_day

    @property
    def set_lamp_max_index(self):
        last_set_day = (self.last_set_date - self.start_date).days
        return last_set_day

    @property
    def set_temp_indexes(self):
        return self.temp_indexes[self.set_temp_min_index:self.set_temp_max_index]

    @property
    def set_lamp_indexes(self):
        return self.lamp_indexes[self.set_lamp_min_index:self.set_lamp_max_index]

    @property
    def dates(self):
        return [self.start_date + timedelta(days=i) for i in range((self.stop_date - self.start_date).days)]

    @property
    def set_dates(self):
        return [d for d in self.dates if self.first_set_date <= d < self.last_set_date]

    @property
    def first_set_date_idx(self):
        return self.dates.index(self.first_set_date)

    @property
    def last_set_date_idx(self):
        return self.dates.index(self.last_set_date)

    def temps2all_temps(self, temps):
        assert len(temps) == len(self.set_temp_indexes)
        first_half = [self.default_temp] * self.set_temp_min_index
        second_half = [self.default_temp] * (len(self.temp_indexes) - self.set_temp_max_index)
        assert len(first_half + temps + second_half) == len(self.temp_indexes)
        return first_half + temps + second_half

    def lamps2all_lamps(self, lamps):
        assert len(lamps) == len(self.set_dates)
        first_half = [self.default_lamp_hour] * self.set_lamp_min_index
        second_half = [self.default_lamp_hour] * (len(self.lamp_indexes) - self.set_lamp_max_index)

        assert len(first_half + lamps + second_half) == len(self.lamp_indexes)
        return first_half + lamps + second_half

    def temps2parameter(self, all_temps):
        """
        inputs: List[float]
        returns: {"01-01": {"0": 20, ... }, "02-01": {"0": 20, ... }}
        """
        assert len(self.dates) * 24 != all_temps
        return_json = {}

        for day_idx, d in enumerate(self.dates):
            day_json = {}
            for hour in range(0, 24, self.hours_period):
                for h_i in range(2):
                    day_json['%s' % (str(hour + h_i),)] = all_temps[day_idx * self.daily_temp_set_point + hour // self.hours_period]

            return_json[date2simulation_str(d)] = day_json

        return return_json

    def lamps2parameter(self, all_lamps):
        """
        inputs: [(12,12), (16,12)] (duration, end_time)
        returns: {"01-01": {"0": 20, ... }, "02-01": {"0": 20, ... }}
        """
        assert len(self.dates) != all_lamps

        hours_light = {}
        end_time = {}
        for idx, d in enumerate(self.dates):
            hours_light[date2simulation_str(d)] = all_lamps[idx]
            end_time[date2simulation_str(d)] = self.default_lamp_end

        return hours_light, end_time
