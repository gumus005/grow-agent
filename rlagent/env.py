from datetime import date, timedelta
from gym import spaces
from rlagent.episodes import outcomes2last_reward, outcomes2last_lamp_state
from rlagent.schedule import Schedule
from rlagent.simulation import get_simulation_results

import numpy as np
import gym


class LampEnv(gym.Env):
    """Custom Environment that follows gym interface"""

    def __init__(self, schedule: Schedule):
        super(LampEnv, self).__init__()
        self._default_schedule = schedule

        self._idx, self._temps, self._lamps = 0, [], []
        self._iterative_schedule = Schedule(self._default_schedule.start_date, self._default_schedule.stop_date, self._default_schedule.first_set_date, self._default_schedule.set_dates[self._idx])

        output = get_simulation_results(self._temps, self._lamps, self._iterative_schedule)

        self._default_obs = outcomes2last_lamp_state(output, self._idx, self._iterative_schedule)

        self.action_space = spaces.Discrete(len(self._default_schedule.lamp_actions))  # Lamp on lamp of
        self.observation_space = spaces.Box(
            np.array([self._default_schedule.min_plant_load, self._default_schedule.min_cumulative_par]),
            np.array([self._default_schedule.max_plant_load, self._default_schedule.max_cumulative_par]),
            dtype=np.float64
        )  # Plant load, cumulative par, par of day,

    def _take_action(self, action):
        self._idx += 1
        self._iterative_schedule = Schedule(self._default_schedule.start_date, self._default_schedule.stop_date, self._default_schedule.first_set_date, self._default_schedule.first_set_date + timedelta(days=self._idx))

        self._temps = [self._iterative_schedule.default_temp] * len(self._iterative_schedule.set_temp_indexes)
        self._lamps += [self._default_schedule.lamp_actions[action]]

    def step(self, action):
        # Execute one time step within the environment
        self._take_action(action)
        output = get_simulation_results(self._temps, self._lamps, self._iterative_schedule)

        obs = outcomes2last_lamp_state(output, self._idx, self._iterative_schedule)
        reward = outcomes2last_reward(output, self._iterative_schedule)

        return obs, reward, len(self._default_schedule.set_dates) == len(self._iterative_schedule.set_dates), {}

    def reset(self):
        # Reset the state of the environment to an initial state
        self._idx, self._temps, self._lamps = 0, [], []

        self._iterative_schedule = Schedule(self._default_schedule.start_date, self._default_schedule.stop_date, self._default_schedule.first_set_date, self._default_schedule.set_dates[self._idx])
        obs = self._default_obs
        return obs


if __name__ == '__main__':
    schedule = Schedule(start_date=date(2017, 12, 15), stop_date=date(2018, 3, 1), first_set_date=date(2018, 2, 1),
                 last_set_date=date(2018, 2, 14))
    env = LampEnv(schedule)
    pls, harvests, done = [hcp], [harvest], False

    while not done:
        print('.')
        (pl, hcp, harvest), reward, done, info = env.step(1)
        pls.append(hcp)
        harvests.append(harvest)

    print()