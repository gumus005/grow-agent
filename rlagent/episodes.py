from rlagent.interpretation import linearized_harvest, flat_plant_load, harvest_cumulative_par, harvest_cumulative_day, \
    harvest_count
from rlagent.schedule import Schedule
from rlagent.simulation import get_simulation_results


def outcomes2temp_episode(outcomes, schedule):
    """
    input: outcomes
    outputs: List[(state, action, reward)]
    """
    prev_states = []
    prev_actions = []
    rewards = []
    states = []
    dones = []
    actions = []

    gains = outcomes['gains']
    harvest = linearized_harvest(outcomes['harvest'], gains)
    total_samples = (schedule.set_temp_max_index + 1) * schedule.hours_period
    harvest = harvest[:total_samples]
    lamp_cost = outcomes['lamp_usage'][:total_samples]
    temp_cost = outcomes['pipe_usage'][:total_samples]
    temps_set_points = outcomes['temps_set_points'][:total_samples]

    for idx in schedule.set_temp_indexes:
        prev_idx = idx
        current_idx = idx + 1

        prev_hour_idx = prev_idx * schedule.hours_period
        current_hour_idx = current_idx * schedule.hours_period

        prev_harvest = harvest[prev_hour_idx]
        current_harvest = harvest[current_hour_idx]

        prev_lamp_cost = lamp_cost[prev_hour_idx]
        current_lamp_cost = lamp_cost[current_hour_idx]

        prev_temp_cost = temp_cost[prev_hour_idx]
        current_temp_cost = temp_cost[current_hour_idx]

        prev_states.append(prev_idx)
        prev_actions.append(temps_set_points[prev_idx])

        states += [current_idx] if not current_idx == schedule.set_temp_max_index else [None]
        actions += [temps_set_points[current_idx]] if not current_idx == schedule.set_temp_max_index else [None]

        rewards.append(((current_harvest - prev_harvest) - (current_lamp_cost - prev_lamp_cost) - (
                    current_temp_cost - prev_temp_cost)))

        dones.append(current_idx == schedule.set_temp_max_index)

    return prev_states, prev_actions, rewards, states, actions, dones


def outcomes2last_reward(outcomes, schedule):
    gains = outcomes['gains']
    harvest = linearized_harvest(outcomes['harvest'], gains)
    lamp_cost = outcomes['lamp_usage']
    temp_cost = outcomes['pipe_usage']

    prev_hour_idx = (schedule.set_lamp_max_index - 1) * 24
    current_hour_idx = schedule.set_lamp_max_index * 24

    prev_harvest = harvest[prev_hour_idx]
    current_harvest = harvest[current_hour_idx]

    prev_lamp_cost = lamp_cost[prev_hour_idx]
    current_lamp_cost = lamp_cost[current_hour_idx]

    prev_temp_cost = temp_cost[prev_hour_idx]
    current_temp_cost = temp_cost[current_hour_idx]

    return (current_harvest - prev_harvest) - (current_lamp_cost - prev_lamp_cost) - (
                    current_temp_cost - prev_temp_cost)


def outcomes2last_lamp_state(outcomes, idx, schedule):
    # Plant load, cumulative par, par of day,
    # gains = outcomes['gains']
    # harvest = linearized_harvest(outcomes['harvest'], gains)
    # harvest = linearized_harvest(outcomes['harvest'], 1)[schedule.set_lamp_max_index * 24]
    hc = harvest_count(outcomes['harvest'], (schedule.set_lamp_max_index + 1)*24)
    pl = flat_plant_load(outcomes['plant_load'])[schedule.set_lamp_max_index * 24]
    hcp = harvest_cumulative_par(outcomes['harvest'], outcomes['par'])[schedule.set_lamp_max_index*24]
    iglob = sum(outcomes['Iglob'][schedule.set_lamp_max_index*24:(schedule.set_lamp_max_index + 1)*24])
    days = harvest_cumulative_day(outcomes['harvest'])[schedule.set_lamp_max_index * 24]

    state = (idx, hc - schedule.min_harvest_count, pl, hcp)
    return state


def outcomes2lamp_episode(outcomes, schedule):
    """
    input: outcomes
    outputs: List[(state, action, reward)]
    """
    prev_states = []
    prev_actions = []
    rewards = []
    states = []
    dones = []
    actions = []

    gains = outcomes['gains']
    harvest = linearized_harvest(outcomes['harvest'], gains)
    total_samples = (schedule.set_lamp_max_index + 1) * 24

    harvest = harvest[:total_samples]
    lamp_cost = outcomes['lamp_usage'][:total_samples]
    temp_cost = outcomes['pipe_usage'][:total_samples]
    lamps_set_points = outcomes['lamps_set_points'][:total_samples // 24]

    for idx in schedule.set_lamp_indexes:
        prev_idx = idx
        current_idx = idx + 1

        prev_hour_idx = prev_idx * 24
        current_hour_idx = current_idx * 24

        prev_harvest = harvest[prev_hour_idx]
        current_harvest = harvest[current_hour_idx]

        prev_lamp_cost = lamp_cost[prev_hour_idx]
        current_lamp_cost = lamp_cost[current_hour_idx]

        prev_temp_cost = temp_cost[prev_hour_idx]
        current_temp_cost = temp_cost[current_hour_idx]

        prev_states.append(prev_idx)
        prev_actions.append(lamps_set_points[prev_idx])

        states += [current_idx] if not current_idx == schedule.set_lamp_max_index else [None]
        actions += [lamps_set_points[current_idx]] if not current_idx == schedule.set_lamp_max_index else [None]

        rewards.append(((current_harvest - prev_harvest) - (current_lamp_cost - prev_lamp_cost) - (current_temp_cost - prev_temp_cost)))

        dones.append(current_idx == schedule.set_lamp_max_index)

    return prev_states, prev_actions, rewards, states, actions, dones


def step(policy, schedule):
    temps, lamps = [], []
    for d in schedule.set_dates:
        iterative_schedule = Schedule(schedule.start_date, schedule.stop_date, schedule.first_set_date, d)
        output = get_simulation_results(temps, lamps, iterative_schedule)

        state = outcomes2last_lamp_state(output, iterative_schedule)
        reward = outcomes2last_reward(output, iterative_schedule)
        action = policy.lamp_act(state, False)

        actions += [action]
        rewards += [reward]

        temps += [schedule.default_temp] * schedule.daily_temp_set_point


    output = get_simulation_results(temps, lamps, schedule)
    lamp_episode = outcomes2lamp_episode(output, schedule)
    return lamp_episode
