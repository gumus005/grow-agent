import json
from datetime import date

from tqdm import tqdm

from rlagent.policy import QLearningAgent, generate_episodes
from rlagent.schedule import Schedule


s = Schedule(start_date=date(2017, 12, 15), stop_date=date(2018, 5, 7), first_set_date=date(2018, 2, 1), last_set_date=date(2018, 3, 15))
q_policy = QLearningAgent(schedule=s)

for i_episode in tqdm(range(1, 500)):
    q_policy.reset_episode()

    temp_episode, lamp_episode, _, _, lamps = generate_episodes(q_policy, schedule=s, rand_lamp=True, rand_temp=False)
    q_policy.learn_lamp(lamp_episode)
    # q_policy.learn_temp_episode(temp_episode)

    # _, _, rewards, _, _, _ = lamp_episode
    # print(q_policy.q_table.lamp_average, sum(rewards) / len(rewards), len(rewards))
    # print(json.dumps(q_policy.q_table.lamp_q_table, indent=2))

    # _, _, rewards, _, _, _ = temp_episode
    # print(q_policy.q_table.temp_average, sum(rewards) / len(rewards), len(rewards))

    if i_episode % 5 == 1:
        temp_episode, lamp_episode, outputs, temps, lamps = generate_episodes(q_policy, schedule=s, rand_lamp=False, rand_temp=False)
        _, _, rewards, _, _, _ = lamp_episode
        print("rewards", lamps, sum(rewards) / len(rewards), len(rewards))