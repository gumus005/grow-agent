import torch

from datetime import date
from torch import optim
from rlagent.env import LampEnv
from rlagent.policy import ReinforceWithBaseline, QLearningAgent
from rlagent.schedule import Schedule

device = torch.device("cpu")


schedule = Schedule(start_date=date(2017, 12, 15), stop_date=date(2018, 5, 1), first_set_date=date(2018, 2, 1),
                 last_set_date=date(2018, 2, 5), max_plant_load=350)

env = LampEnv(schedule)

agent = QLearningAgent(schedule)

n_episodes = 500

scores = []
for i_episode in range(1, n_episodes + 1):
    actions = []; rewards = []; observations = []
    done = False

    obs = env.reset()
    while not done:
        observations.append(obs)

        action = agent.lamp_act(obs)
        actions.append(action)

        obs, reward, done, _ = env.step(action)
        rewards.append(reward)

    for idx, (obs, action) in enumerate(zip(observations, actions)):
        agent.learn_lamp(obs, action, sum(rewards[idx:]))

    print('Episode: %s, Score: %s' % (i_episode, sum(rewards)))

print()
